from django.shortcuts import render
# from AjaxDjango.enroll.models import User
from .forms import StudentRegistration
from .models import User
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt    # To remove(exclude) csrf_token 
from django.contrib.auth.hashers import make_password
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
# Create your views here.

def home(request):
    form = StudentRegistration()
    return render (request, 'home.html', {
        'form':form})

# from django.db import connection
def fetch_data(request):
    # cursor = connection.cursor()
    # cursor.execute("SELECT * FROM enroll_user ORDER BY id")
    # student_data = cursor.fetchall()

    student_data = User.objects.all().values().order_by('id')
    paginator = Paginator(student_data, 5)  # Show 5 records per page

    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    student_data = list(page_obj)  # Convert the queryset to a list

    return JsonResponse({'student_data': student_data, 'total_pages': paginator.num_pages})

# def fetch_data(request):
#     student_data = list(User.objects.values())
#     return JsonResponse({'student_data': student_data})


def save_data(request):
    if request.method == "POST":
        form = StudentRegistration(request.POST)
        # print(form.data)
        if form.is_valid():
            sid = request.POST.get('stuid')   # FOR UPDATE DATA
            name = request.POST['name']
            email = request.POST['email']
            password = request.POST['password']
            
            # Encrypt the password
            # password = make_password(password)

            if(sid == ''):
                usr = User(name=name, email=email,password=password)
            else:
                usr = User(id=sid, name=name, email=email,password=password)
            
            usr.save()
            # stud = User.objects.values()
            # student_data = list(stud)
            # print(student_data)
            return JsonResponse({'status' : 'Save'})
        else:
            return JsonResponse({'status':  0})

def delete_data(request):
    if request.method == "POST":
        id = request.POST.get('sid')
        # print(id)
        pi = User.objects.get(pk=id)
        pi.delete()
        return JsonResponse({'status' : 1})
    else:
        return JsonResponse({'status': 0})


def edit_data(request):
    if request.method == "GET":
        id = request.GET.get('sid')
        # print(id)
        student = User.objects.get(pk=id)
        student_data = {"id":student.id, "name" : student.name, "email": student.email, "password": student.password}
        return JsonResponse(student_data)
    if request.method == "POST":
        form = StudentRegistration(request.POST)
        if form.is_valid():
            sid = request.POST.get('stuid')   # FOR UPDATE DATA
            name = request.POST['name']
            email = request.POST['email']
            password = request.POST['password']
            
            usr = User(id=sid, name=name, email=email,password=password)
            usr.save()
            stud = User.objects.filter(id = sid).values()
            student_data = list(stud)
            return JsonResponse({'status' : 'Updated', 'student_data': student_data})
        else:
            return JsonResponse({'status':  0})    