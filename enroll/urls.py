from enroll import views
from django.urls import path

app_name = "enroll"

urlpatterns = [
    path('', views.home, name='home'),
    path('fetch_data/', views.fetch_data, name='fetch_data'),
    path('save/', views.save_data, name='save'),
    path('edit/', views.edit_data, name='edit'),
    path('delete/', views.delete_data, name='delete'),
]
